
// Global vars
var menu;
var counter;
var int_menu;
var int_state;

window.activate_menu_slider =

    function(c) {

         // Set attributes
         menu = utils.split_string(c.items);

         // Append menu items
         // console.log($("#menu_slider .menu_overlay"));
         for(var i = 0; i < menu.length; i++) {
            $("#menu_slider .menu_overlay").append("<div class='left menu'><div><h1>"+menu[i]+"</h1></div></div>");
         }

         counter = 0;
         int_menu = setInterval(animate_menu,300);
         int_state = setInterval("animate_menu_state('start')",450);

         $('.menu').click(function(e) {

             var target = $(e.target).parent().parent().index();

             if($(e.target).parent().parent().hasClass('menu') == true) {
                 animate_menu_state(target, $(e.target).parent().parent().position().left);
             }

         });

    }

function animate_menu() {

    var element_to_animate = $("#menu_slider .menu_overlay > DIV > DIV").eq(counter);

    counter += 1;

    var tp = create_instance();

    tp['left'].start = 120;
    tp['left'].duration = 1.2;
    tp['opacity'].stop = 100;
    tp['onStop'] = function(){if(counter > 2) {clearInterval(int_menu)}}

    tween_this(element_to_animate, tp);

}

function animate_menu_state(state,direction) {

    var start_pos = 0;
    var stop_pos = 0;
    var start_op = 0;
    var stop_op = 100;

    if(state == 'start') {
    start_pos = -180;
    clearInterval(int_state);
    }

    if(state != 'start') {

     var index = Number(state);

     start_pos = $('.menu_state').position().left;
     start_op = 100;
     stop_op = 100;

     // Move state depending on index

    if(Number(index)) {

        if(direction > $('.menu_state').position().left) {
            // Going right
            stop_pos = direction;
        } else {
            // Going left
            stop_pos = direction;
        }

    }

    }

    var tp = create_instance();

    tp['left'].start = start_pos;
    tp['left'].stop = stop_pos;
    tp['left'].duration = 1.2;
    tp['opacity'].start = start_op;
    tp['opacity'].stop = stop_op;

    tween_this($('.menu_state'), tp);

}