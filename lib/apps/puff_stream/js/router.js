var AppRouter = Backbone.Router.extend({

    routes: {
        "*action" : "default"
    },

    initialize: function () {
        // Because the header may change depending on Puff Stream place, e.g.
        // dashboard, docs, cloud, photos etc.., header will be introduced
        // throughout each of the methods below...
    },

	default: function(action) { // Default Index
	    console.log("Default Router Started");

	    // Build and display 'App Interface'

        if (!this.app_page) {
            this.app_page = new utils.build_page('appInterface');
        } else {
            console.log(this.app_page + " exist");
        }

    }

});

new AppRouter();

Backbone.history.start();