// Puff Stream app
// Inc. Menu Slider

window.PuffStream = Backbone.Model.extend({
    // Nothing needs to be in here at this stage
    defaults: function() {
        return {};
    }
});

window.AppView = Backbone.View.extend({

    initialize:function () {
        this.render();
    },

    render:function () {
        $(this.el).html(this.template());
        return this;
    }

});
