
// Utils will hold various functions
// The first is to build specific pages (Puff Stream 'Places') from various templates

window.page = {
    app: {
        config: config.app,
        views: 'menu_slider'
    },
    doc:"",
    dashboard:""
}

window.utils = {

    build_page: function(p) {

        var deferreds = [];
        var config = page.app.config;
        var views;
        var page_view = "";
        var template = "";
        var count = 0;

        // for each view build page
        if(p == "appInterface") {
            console.log('Build Page');
            views = utils.split_string(page.app.views);
            page_view = "AppView";
        }

        count = views.length;

        $.each(views, function(index, view) {
            deferreds.push($.get('lib/apps/puff_stream/tpl/' + view + '.html', function(data) {

                console.log("Build Template");
                template += data; // Add data to template

                if(!--count) utils.display_page(page_view, template, config);

            }));
        });

    },

    display_page: function(view, template, config) {

        window[view].prototype.template = _.template(template);

        console.log("Display View");
        $('#puffstream_app').html(new window[view]().render().el);

        config.initiate(config);

    },

    split_string: function(string) {
        return string.split(",");
    }

}