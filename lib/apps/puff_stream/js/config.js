
// Menu Slider Configurations
window.config = {

    app: {
        items: 'Apps,Projects,Feeds',
        initiate: function(c) {

            // Activate Menu Slider
            activate_menu_slider(c);

        }
    }

}