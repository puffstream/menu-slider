
// tween.js further extends/abstracts Tween JS

var tween_prop = {
    left: {},
    opacity: {},
    onStop: function() {}
}
var left = { // default props
    start: 0,
    stop: 0,
    time: 0,
    units: 'px',
    duration: 1,
    effect:'easeInOut'
}
var opacity = { // default probs
    start: 0,
    stop: 0,
    time: 0,
    duration: 1,
    effect:'easeInOut'
}

// Tween this
function tween_this(element, tween) {
    $(element).tween(tween);
    $.play();
}

function create_instance() {

    var tp = tween_prop;

    var left_instance = left, opacity_instance = opacity;

    tp['left'] = left_instance;
    tp['opacity'] = opacity_instance;

    return tp;

}

// Function for adding properties
// add properties of Obj2 to Obj1
// http://www.dyn-web.com/tutorials/obj_lit.php
function augment(Obj1, Obj2) {
    var prop;
    for ( prop in Obj2 ) {
        if ( Obj2.hasOwnProperty(prop) && !Obj1[prop] ) {
            Obj1[prop] = Obj2[prop];
        }
    }
}