## Synopsis

Part of Puff Stream App. Menu Slider is a 3-tier feature menu app/application. Developed with backbone.js, includes an abstraction of JS Tween.

## Code Example

Menu Slider code is developed with backbone.js, underscore.js, jquery and compliant HTML/CSS.

Example code from main.js

    window.activate_menu_slider =

    function(c) {

         // Set attributes
         menu = utils.split_string(c.items);

         // Append menu items
         // console.log($("#menu_slider .menu_overlay"));
         for(var i = 0; i < menu.length; i++) {
            $("#menu_slider .menu_overlay").append("<div class='left menu'><div><h1>"+menu[i]+"</h1></div></div>");
         }

         counter = 0;
         int_menu = setInterval(animate_menu,300);
         int_state = setInterval("animate_menu_state('start')",450);

         $('.menu').click(function(e) {

             var target = $(e.target).parent().parent().index();

             if($(e.target).parent().parent().hasClass('menu') == true) {
                 animate_menu_state(target, $(e.target).parent().parent().position().left);
             }

         });

    }

## Motivation

Develop a series of apps/applications which are extendable inside the Puff Stream App.

## Installation

Installation is easy. Drag and drop the entire folder into your host root directory. After upload is complete, browse to index.html.

E.g. http://www.yourdomain.com/menuslider/index.html.

## Contributors

* www.puffstream.com
* https://twitter.com/jamesstar8

## License

General Public License
